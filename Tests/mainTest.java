import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class mainTest {
    @Test
    public void testBookStoreBasic_ZeroBooks_ShouldReturn8(){
        //Act
        int[] books = new int[]{};
        //Arrange
        double expected = 0;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreBasic_OneBook1_ShouldReturn8(){
        //Act
        int[] books = new int[]{1};
        //Arrange
        double expected = 8;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreBasic_OneBook2_ShouldReturn8(){
        //Act
        int[] books = new int[]{2};
        //Arrange
        double expected = 8;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreBasic_OneBook3_ShouldReturn8(){
        //Act
        int[] books = new int[]{3};
        //Arrange
        double expected = 8;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreBasic_3Book1_ShouldReturn24(){
        //Act
        int[] books = new int[]{1,1,1};
        //Arrange
        double expected = 24;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreDiscount_2Books1and2_ShouldApplyCorrectDiscount(){
        //Act
        int[] books = new int[]{1,2};
        //Arrange
        double expected = 8 * 2 * 0.95;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreBasic_3Books0and2and3_ShouldApplyCorrectDiscount(){
        //Act
        int[] books = new int[]{0,2,3};
        //Arrange
        double expected = 8 * 3 * 0.9;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreBasic_4Books0and1and2and3_ShouldApplyCorrectDiscount(){
        //Act
        int[] books = new int[]{0,1,2,3};
        //Arrange
        double expected = 8 * 4 * 0.8;
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreMultipleDiscounts_3Books0and0and1_ShouldApplyCorrectDiscount(){
        //Act
        int[] books = new int[]{0,0,1};
        //Arrange
        double expected = (8 + (8 * 2 * 0.95));
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreMultipleDiscounts_4Books0and0and1and1_ShouldApplyCorrectDiscount(){
        //Act
        int[] books = new int[]{0,0,1,1};
        //Arrange
        double expected = (2 * (8 * 2 * 0.95));
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void testBookStoreMultipleDiscounts_6Books0and0and1and2and2and3_ShouldApplyCorrectDiscount(){
        //Act
        int[] books = new int[]{0,0,1,2,2,3};
        //Arrange
        double expected = ((8 * 4 * 0.8) + (8 * 2 * 0.95));
        double actual = main.CalculatePrice(books);
        //Assert
        assertEquals(expected, actual);
    }


}