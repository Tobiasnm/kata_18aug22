import java.util.*;

public class main {
    public static void main(String[] args) {
        int[] asd = new int[]{1,3};
        System.out.println(CalculatePrice(asd));
    }

    public static double CalculatePrice(int[] books) {
        HashMap<Integer, Integer> Items;
        HashMap<Integer, Integer> TempItems;

        double price = 0;
        if (books.length == 0) return 0; //if no books return 0
        else {
            Items = new HashMap<>();
            for (int a : books) { //count occurences of books and store in hashmap
                Items.put(a, Items.get(a) == null ? 1 : Items.get(a)+1);
            }

            while(!Items.isEmpty()){ //runs while there are still books in the hashmap
                int counter = 0;
                int current;
                TempItems = new HashMap<>(Items); //makes a temporary copy
                for (int i : Items.keySet()) { //loops through books
                    counter++; //counts each occurence of unique books
                    current = TempItems.get(i); //saves value
                    if(current == 1) //if there is only 1 of the book remove the value from hashmap
                        TempItems.remove(i);
                    else //if there is more than 1 book decrement by 1
                        TempItems.put(i, current - 1);
                }
                Items = new HashMap<>(TempItems); //copy the temp hashmap back to original
                switch (counter){ //4 different cases for different discounts based on amount of unique books
                    case 1:
                        price += 8;
                        break;
                    case 2:
                        price += 2 * 8 * 0.95;
                        break;
                    case 3:
                        price += 3 * 8 * 0.9;
                        break;
                    case 4:
                        price += 4 * 8 * 0.8;
                        break;
                }
            }
            return price;
        }
    }
}
